# WebApp for creating Strategic Indicators using Bayesian Networks - Q-Rapids Strategic Dashboard ![](https://img.shields.io/badge/License-Apache2.0-blue.svg) [![Build Status](https://travis-ci.org/q-rapids/qrapids-dashboard.svg?branch=master)](https://travis-ci.org/q-rapids/qrapids-dashboard) [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=q-rapids_qrapids-dashboard&metric=alert_status)](https://sonarcloud.io/dashboard?id=q-rapids_qrapids-dashboard)
Degree Final Project (TFG) at Facultat d'Informàtica de Barcelona (FIB - UPC), for Computer Science (Software Engineering).

In software projects, obtaining adequate Strategic Indicators is a goal that all developers and companies try to achieve in order to improve the life cycle and quality of the project. The goal of this thesis is to add a new functionality to the dashboard of the Q-Rapids program (Quality-Aware Rapid Software Development)  that allows entering the necessary information to create the Bayesian networks that will be used to compute Strategic Indicators. This information will help in defining the Strategic Indicator itself, as well as creating the associated Bayesian network.

This is intended to provide a new element in the dashboard to facilitate obtaining quality Strategic Indicators and also to reduce dependence on external software.

This component has been created as a result of the Q-Rapids project funded by the European Union Horizon 2020 Research and Innovation programme under grant agreement No 732253.

## Main Functionality
The **User's Guide** is available in the [Wiki](https://github.com/q-rapids/qrapids-dashboard/wiki/User-Guide).

## Technologies
|Property|Description|
| -------------------- | --------------------------------|
| Type of component    | Web Application                 |
| Build                | .war                            |
| Programming language | Java                            |
| DBMS                 | PostgreSQL                       |
| Frameworks           | Spring Boot, AngularJS, Gradle  |
| External libraries   | Chart.js, ElasticSearch java API|

## How to build
This is a Gradle project. You can use any IDE that supports Gradle to build it, or alternatively you can use the command line using the Gradle wrapper with the command *__gradlew__* if you don't have Gradle installed on your machine or with the command *__gradle__* if you do, followed by the task *__war__*.

```
# Example: using Gradle wrapper to build with dependencies
cd qrapids-dashboard
gradlew war
```
After the build is done the WAR file can be found at the __build/libs__ directory

## Documentation

You can find the user documentation in the repository [Wiki](https://github.com/q-rapids/qrapids-dashboard/wiki) and the technical documentation of the RESTful API [here](https://q-rapids.github.io/qrapids-dashboard).

## Contributing

You can find guidelines to contribute to the project in the [Q-Rapids repository](https://github.com/q-rapids/q-rapids/blob/master/CONTRIBUTING.md).

## Licensing

Software licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)
 
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
